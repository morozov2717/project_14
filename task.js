const fs = require('fs')

fs.readFile('./text.txt','utf8',(error, data)=>{
    console.log('Данные прочитаны успешно')
    fs.mkdirSync('./folder')
    console.log('Директория folder создана')
    fs.writeFileSync('./folder/text_2.txt', data)
    console.log('Данные успешно записаны')
})
setTimeout(()=>{
    fs.unlink('./folder/text_2.txt', ()=>{console.log('Файл text_2.txt удалён')})
}, 5000)